package fr.greta.formation.dglv.prepandroobserver.PrepAndro_Observer;

public abstract class Observer {
	   protected Eleve eleve;
	   public abstract void update();
	}
